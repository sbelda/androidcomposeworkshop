package com.sergiobelda.composeworkshop.model

data class Song(
    val id: Long,
    val number: Int,
    val title: String,
    val artistId: Long,
    val saved: Boolean
)
